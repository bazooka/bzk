/*
 * @title Twig
 * @description Compile your Twig to HTML !
 */

// Dependencies
const gulp = require('gulp');
const plumber = require('gulp-plumber');
const dotenv = require('dotenv');
const data = require('gulp-data');
const twig = require('gulp-twig');
const fs = require('fs');
const path = require('path');

// Config
dotenv.config();

// Consts
const TWIG_SRC = process.env.TWIG_SRC ? process.env.TWIG_SRC : `${process.env.SRC}/**/*.twig`;
const TWIG_DATA_SRC = `${process.env.SRC}/**/*.twig.json`;
const TWIG_DEST = process.env.TWIG_DEST ? process.env.TWIG_DEST : process.env.DEST;

// Task
function compileTwig() {
  return gulp.src(TWIG_SRC)
    // On error
    .pipe(plumber({
      errorHandler: function (err) {
        console.log('\x1b[31m', err.message);
        this.emit('end');
      }
    }))
    // Get variables from .json file to compile
    .pipe(data(function (file) {

      let twigDataFilePath = path.dirname(file.path) + '/' + path.basename(file.path) + '.json';
      if (fs.existsSync(twigDataFilePath)) {
        return JSON.parse(fs.readFileSync(twigDataFilePath));
      }

      return
    }))
    .pipe(twig())
    // If error during Twig compilation
    .on('error', function (err) {
      process.stderr.write(err.message + '\n');
      this.emit('end');
    })
    .pipe(gulp.dest(TWIG_DEST))
}

exports.compileTwig = compileTwig;
exports.TWIG_SRC = TWIG_SRC;
exports.TWIG_DATA_SRC = TWIG_DATA_SRC;
exports.TWIG_DEST = TWIG_DEST;
