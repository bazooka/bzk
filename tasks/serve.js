/*
 * @title Server
 * @description A task to initialise a local server
 */

// Dependencies
const gulp = require('gulp');
const browserSync = require('browser-sync');
const dotenv = require('dotenv');

// Imports
const {compileHtml, HTML_SRC} = require('./html');
const {compileTwig, TWIG_SRC, TWIG_DATA_SRC} = require('./twig');
const {compileSass, SASS_SRC} = require('./styles');
const {scripts, JS_SRC} = require('./scripts');
const {optimizeImages, IMG_SRC} = require('./images');
const {convertFonts, FONT_SRC} = require('./fonts');

// Config
dotenv.config();

// Tasks
function serve(callback) {
  let options = {};

  if (process.env.URL) {
    options = {
      proxy: process.env.URL,
      notify: true
    };
  } else {
    options = {
      server: {
        baseDir: [process.env.DEST]
      },
      notify: true
    }
  }

  browserSync.init(options);
  watch();
  callback();
}

function reload(callback) {
  browserSync.reload();
  callback();
}

function watch() {
  gulp.watch(HTML_SRC, gulp.series(compileHtml, reload));
  gulp.watch(TWIG_SRC, gulp.series(compileTwig, reload));
  gulp.watch(TWIG_DATA_SRC, gulp.series(compileTwig, reload));
  gulp.watch(SASS_SRC, gulp.series(compileSass));
  gulp.watch(JS_SRC, gulp.series(scripts, reload));
  gulp.watch(IMG_SRC, gulp.series(optimizeImages, reload));
  gulp.watch(FONT_SRC, gulp.series(convertFonts, reload));
}

exports.serve = serve;
