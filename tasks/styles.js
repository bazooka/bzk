/*
 * @title Sass
 * @description Compile your Sass !
 */

// Dependencies
const gulp = require('gulp');
const gulpIf = require('gulp-if');
const sourcemaps = require('gulp-sourcemaps');
const sass = require('gulp-sass');
const sassTilde = require('node-sass-tilde-importer')
const postcss = require('gulp-postcss');
const postcssNormalize = require('postcss-normalize');
const postcssPresetEnv = require('postcss-preset-env');
const cssNano = require('cssnano');
const browserSync = require('browser-sync');
const dependents = require('gulp-dependents');
const Fiber = require('fibers');
const dotenv = require('dotenv');
const isProd = require('../gulpfile').isProd;

// Config
dotenv.config();

// Consts
const SASS_SRC = process.env.SASS_SRC ? process.env.SASS_SRC : `${process.env.SRC}/**/*.scss`;
const SASS_DEST = process.env.SASS_DEST ? process.env.SASS_DEST : process.env.DEST;

// Task
function compileSass() {
  return gulp.src(SASS_SRC, { since: gulp.lastRun(compileSass), sourcemaps: isProd})
    .pipe(dependents())
    .pipe(gulpIf(isProd, sourcemaps.init()))
    .pipe(sass.sync({
      includePaths: [
        'node_modules'
      ],
      importer: sassTilde,
      fiber: Fiber
    }))
    .pipe(postcss([
      postcssNormalize(), // Add css normalizer by default
      postcssPresetEnv({ // Enable future CSS features
        stage: 2,
        browsers: ['last 3 versions'],
        autoprefixer: {grid: true} // Add vendor prefix to CSS rules by Can I Use and also grid
      }),
      cssNano() // Minify CSS
    ]))
    .pipe(gulpIf(isProd, sourcemaps.write()))
    .pipe(gulp.dest(SASS_DEST))
    .pipe(browserSync.stream());
}

exports.compileSass = gulp.series(compileSass);
exports.SASS_SRC = SASS_SRC;
exports.SASS_DEST = SASS_DEST;
