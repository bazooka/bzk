#!/usr/bin/env node
'use strict';

// Dependencies
const fs = require('fs');

// TODO: Check if files exists and copy it if needed
/* Copy .ENV, .babelrc and eslint files */
function copyInitialFiles(wo) {

  if (isWordPress()) {
    console.log("Copying WordPress configuration files");
  } else {
    console.log("Copying default configuration files");
  }

  // .env
  fs.stat('./.env', (err) => {
    if (err) {
      fs.copyFile(`${__dirname}/../example.env`, './.env', (err) => {
        if (err) throw err;
        console.log('.env was copied to the root');
      });
    } else {
      fs.readFile(`${__dirname}/../example.env`, 'utf8', (err, data) => {
        if (err) throw err;

        console.log('Appending BZK CLI variables to your .env')
        fs.appendFileSync('./.env', `\n\n${data}`);
      })
    }
  });

  // GitIgnore
  fs.stat('./.gitignore', (err) => {
    if (err) {
      fs.copyFile(`${__dirname}/../example.gitignore`, './.gitignore', (err) => {
        if (err) throw err;
        console.log('.gitignore was copied to the root');
      });
    } else {
      fs.readFile(`${__dirname}/../example.gitignore`, 'utf8', (err, data) => {
        if (err) throw err;

        console.log('Appending BZK CLI Gitignore to your .gitignore')
        fs.appendFileSync('./.gitignore', `\n\n${data}`);
      })
    }
  });

  const files = ['.editorconfig', '.eslintignore', '.eslintrc'];

  files.forEach((filename) => {
    checkCopyOrRemoveFile(filename)
  });
}

function checkCopyOrRemoveFile(filename) {
  if (isWordPress()) {
    var filePath = `${__dirname}/../wordpress/${filename}`;
  } else {
    var filePath = `${__dirname}/../${filename}`;
  }

  fs.stat(filename, (err) => {
    if (err) {
      fs.copyFile(filePath, filename, (err) => {
        if (err) throw err;
        console.log(`${filename} was copied to the root`);
      });
    }
  });
}

function isWordPress() {
  return process.argv.includes('--wordpress');
}

console.log('\n⚡️ Bazooka');
console.log('Hello Renegade Developer 🚀\n');

copyInitialFiles();
