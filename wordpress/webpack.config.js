const defaultConfig = require( '@wordpress/scripts/config/webpack.config' )

/*
* @title Webpack Config
*/
const mode = process.env.NODE_ENV === 'prod' ? 'production' : 'development'
const { resolve, optimization } = defaultConfig

// Webpack
module.exports = {
  mode,
  resolve,
  optimization,
  module: defaultConfig.module
}
