/*
 * @title gulpfile.babel.js
 * @description A directory file loader to include all the gulp tasks
 */

// Dependencies
const gulp = require('gulp')

// Tasks
const {serve} = require('./tasks/serve')
const {clean} = require('./tasks/clean')
const {compileHtml} = require('./tasks/html')
const {compileTwig} = require('./tasks/twig')
const {compileSass} = require('./tasks/styles')
const {scripts} = require('./tasks/scripts')
const {optimizeImages} = require('./tasks/images')
const {convertFonts} = require('./tasks/fonts')
const {runRevision} = require('./tasks/revision')

// Scripts
const build = gulp.series(
  clean,
  gulp.series(
    gulp.parallel(compileHtml, compileTwig, compileSass, scripts, optimizeImages, convertFonts),
    runRevision
  )
)

const dev = gulp.series(
  clean,
  gulp.parallel(compileHtml, compileTwig, compileSass, scripts, optimizeImages, convertFonts),
  serve
)

exports.build = build
exports.dev = dev
exports.isProd = process.env.NODE_ENV === 'prod'