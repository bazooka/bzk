/*
 * @title Webpack Config
 */
const mode = process.env.NODE_ENV === 'prod' ? 'production' : 'development'

// Webpack
module.exports = {
  mode,
  module: {
    rules: [
      {
        test: /^(?!.*\.{test,min}\.js$).*\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      }
    ]
  }
}
